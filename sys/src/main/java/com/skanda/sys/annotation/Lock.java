package com.skanda.sys.annotation;

import java.lang.annotation.*;

/**
 * 分布式锁注解
 *
 */

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Lock {
    //锁名
    String key();
    String value() default "lock";
}
