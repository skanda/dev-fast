package com.skanda.sys.exception;

/**
 * 业务异常
 */
public class BizException extends RuntimeException {

    public static final BizException UNFINISH_REQUEST = new BizException(1000,"请求未结束,请稍后处理");

    /**
     * 异常信息
     */
    protected String msg;

    /**
     * 具体异常码
     */
    protected int code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public BizException(int code, String msg) {
        this.msg = msg;
        this.code = code;
    }
}
