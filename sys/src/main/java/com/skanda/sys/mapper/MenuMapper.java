package com.skanda.sys.mapper;

import com.skanda.sys.domain.SkMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Entity com.skanda.sys.domain.SkMenu
 */
@Mapper
public interface MenuMapper extends BaseMapper<SkMenu> {

    List<SkMenu> getRoutesByRole(String role);
}




