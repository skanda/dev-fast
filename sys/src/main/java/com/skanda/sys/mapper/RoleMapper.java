package com.skanda.sys.mapper;

import com.skanda.sys.domain.SkRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.skanda.sys.domain.SkRole
 */
@Mapper
public interface RoleMapper extends BaseMapper<SkRole> {

}




