/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.skanda.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.skanda.common.entity.User;
import com.skanda.sys.domain.SkRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Mapper 接口
 *
 * @author Chill
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


	/**
	 * 获取用户
	 *
	 * @param account
	 * @param password
	 * @return
	 */
	@Select("select *  from sk_user where username=#{account} and password=#{password}")
	User getUser(@Param("account") String account,@Param("password") String password);

}
