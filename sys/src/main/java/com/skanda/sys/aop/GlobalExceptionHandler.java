package com.skanda.sys.aop;

import com.skanda.common.entity.R;
import com.skanda.sys.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BizException.class)
    public R BizExceptionHanler(BizException e){
        log.error(e.getMessage());
        return R.fail(e.getMsg());
    }
}
