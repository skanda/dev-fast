package com.skanda.sys.domain;

import java.io.Serializable;
import lombok.Data;

/**
 * 角色表
 * @TableName sk_role
 */
@Data
public class SkRole implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 父主键
     */
    private Long parentId;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 
     */
    private String roleEnName;

    /**
     * 是否已删除
     */
    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}