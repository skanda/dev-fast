package com.skanda.sys.domain;

import java.io.Serializable;
import lombok.Data;

/**
 * 菜单表
 * @TableName sk_menu
 */
@Data
public class SkMenu implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 父级菜单
     */
    private Long parentId;

    /**
     * 菜单编号
     */
    private String code;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单别名
     */
    private String alias;

    /**
     * 请求地址
     */
    private String path;

    /**
     * 菜单资源
     */
    private String skIcon;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 是否打开新页面
     */
    private Integer isOpen;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否已删除
     */
    private Integer isDeleted;

    private static final long serialVersionUID = 1L;
}