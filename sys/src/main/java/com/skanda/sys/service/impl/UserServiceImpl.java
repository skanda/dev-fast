/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.skanda.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.skanda.sys.domain.SkRole;
import com.skanda.sys.mapper.RoleMapper;
import com.skanda.sys.mapper.UserMapper;
import com.skanda.sys.service.IUserService;
import com.skanda.common.entity.User;
import com.skanda.common.entity.UserInfo;
import lombok.AllArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务实现类
 *
 * @author Chill
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

	@Autowired
	UserMapper userMapper;
	@Autowired
	RoleMapper roleMapper;

	@Override
	public UserInfo userInfo(Long userId) {
		return null;
	}

	@Override
	public UserInfo userInfo(String account, String password) {
		UserInfo userInfo = new UserInfo();
		User user = userMapper.getUser(account,password);
		userInfo.setUser(user);
		if (user!=null) {
			SkRole userRole = roleMapper.selectById(user.getRoleId());
			List<String> roleList = new ArrayList<>();
			roleList.add(userRole.getRoleEnName());
			userInfo.setRoles(roleList);
		}
		return userInfo;
	}
}
