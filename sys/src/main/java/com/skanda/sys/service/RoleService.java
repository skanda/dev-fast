package com.skanda.sys.service;

import com.skanda.sys.domain.SkRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface RoleService extends IService<SkRole> {

}
