package com.skanda.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.skanda.sys.domain.SkMenu;
import com.skanda.sys.service.IMenuService;
import com.skanda.sys.mapper.MenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, SkMenu>
    implements IMenuService {

    @Autowired
    MenuMapper menuMapper;

    @Override
    public List<SkMenu> getRoutesByRole(String roleName) {
        return menuMapper.getRoutesByRole(roleName);
    }
}




