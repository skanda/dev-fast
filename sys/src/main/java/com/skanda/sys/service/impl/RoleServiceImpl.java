package com.skanda.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.skanda.sys.domain.SkRole;
import com.skanda.sys.service.RoleService;
import com.skanda.sys.mapper.RoleMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, SkRole>
    implements RoleService {

}




