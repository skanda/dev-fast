package com.skanda.sys.service;

import com.skanda.sys.domain.SkMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 *
 */
public interface IMenuService extends IService<SkMenu> {

    List<SkMenu> getRoutesByRole(String role_name);
}
