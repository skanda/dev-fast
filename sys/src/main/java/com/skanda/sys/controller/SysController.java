package com.skanda.sys.controller;

import com.skanda.common.entity.R;
import com.skanda.common.util.WebUtil;
import com.skanda.sys.annotation.Lock;
import com.skanda.sys.domain.SkMenu;
import com.skanda.sys.service.IMenuService;
import com.skanda.sys.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@Slf4j
@Api(tags = "系统模块")
@RequestMapping("sys")
@RestController
@AllArgsConstructor
public class SysController {

    //@Autowired
    IUserService userService;
    //@Autowired
    IMenuService menuService;

    @GetMapping("/test")
    @Lock(key = "testLock")
    public String test(HttpServletRequest request) throws InterruptedException {
        System.out.println(WebUtil.getUserId()+"---");
        Thread.sleep(5000);
        return "testaaa12"+request.getHeader("role_name");
    }

    @Lock(key = "testLock")
    @ApiOperation(value = "获取系统参数")
    @GetMapping("getSysParams")
    public String getSysParams() throws InterruptedException {

        log.info("asdf");
        Thread.sleep(5000);
        return "asdf";
    }

    @ApiOperation(value = "获取路由信息")
    @GetMapping("routes")
    public R<List<SkMenu>> routes(HttpServletRequest httpRequest){
        System.out.println(WebUtil.getUserRoleName());
        return R.data(menuService.getRoutesByRole(WebUtil.getUserRoleName()));
    }
}
