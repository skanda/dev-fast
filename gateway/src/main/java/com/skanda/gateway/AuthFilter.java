package com.skanda.gateway;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.discovery.CommonConstants;
import com.skanda.common.Constants.CommonConstant;
import com.skanda.gateway.prop.AuthProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.Charsets;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.cert.ocsp.Req;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.AuthProvider;
import java.util.*;
import java.util.function.Consumer;

@Slf4j
@Component
@AllArgsConstructor
public class AuthFilter implements GlobalFilter, Ordered {

    private final ObjectMapper objectMapper;
    private final AuthProperties authProperties;
    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String path = exchange.getRequest().getURI().getPath();
        if(isSkip(path)){
            return  chain.filter(exchange);
        }
        ServerHttpResponse resp = exchange.getResponse();
        String headerToken = exchange.getRequest().getHeaders().getFirst(CommonConstant.AUTH_KEY);
        if (StringUtils.isBlank(headerToken) ) {
            return unAuth(resp, "缺失令牌,鉴权失败");
        }
        Claims claims = null;
        try {
            claims = Jwts.parserBuilder()
                    .setSigningKey(CommonConstant.SIGN_KEY.getBytes(Charsets.UTF_8)).build()
                    .parseClaimsJws(headerToken).getBody();
        }catch (Exception e){
            return unAuth(resp, "请求未授权");
        }
        if (claims == null || claims.get("user_id")==null) {
            return unAuth(resp, "请求未授权");
        }
        //String userId = (String) claims.get("user_id");
        //String roleId = (String)claims.get("role_id");
        //String roleName = (String)claims.get("role_name");
        return chain.filter(exchange);
    }
    private Mono<Void> unAuth(ServerHttpResponse resp, String msg) {
        resp.setStatusCode(HttpStatus.UNAUTHORIZED);
        resp.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        String result = "";
        try {
            Map<String, Object> map = new HashMap<>(16);
            map.put("code", 401);
            map.put("msg", msg);
            map.put("data", null);
            result = objectMapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
        DataBuffer buffer = resp.bufferFactory().wrap(result.getBytes(StandardCharsets.UTF_8));
        return resp.writeWith(Flux.just(buffer));
    }

    @Override
    public int getOrder() {
        return 0;
    }

    public boolean isSkip(String path){
       return authProperties.getSkipUrl().stream().anyMatch(pattern ->{return antPathMatcher.match(pattern,path);});
    }
}