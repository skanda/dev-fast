package com.skanda.common.Constants;

public interface CommonConstant {

    /**
     * 默认成功编码
     */
    int DEFAULT_SUCCESS_CODE = 200;

    /**
     * 默认成功消息
     */
    String DEFAULT_SUCCESS_MESSAGE = "操作成功";
    String SIGN_KEY = "1Z~Vs*1Eu3txiWzX$*3z&oLM05KAwRWw^dsQgcL4I)#QzLgzvFe28P(1#VyC#goPILW)0aUvRXIxhVo&P8tSjO0km)iaX$QMw!8Jyz@S%)4NWC!BN)Z@fwrOh7tQ9EdU#9Vh+gUhBnR2T%GS+Ziw%%gYTTvkBBV~ioi#FmYyve7@Vk&55KK2jTbzsw4OLg^x2lHh1j!F7H33E@kngGS4!!lmy8qjJPqBzfHy!XI@!02t0L~G8HnUQUL)smy9RGN^";
    String USER_ID = "user_id";
    String ROLE_ID = "role_id";
    String DEPT_ID = "dept_id";
    String ACCOUNT = "account";
    String USER_NAME = "user_name";
    String ROLE_NAME = "role_name";
    String AUTH_KEY = "sk_auth";
}
