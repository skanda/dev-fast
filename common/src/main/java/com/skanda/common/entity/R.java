package com.skanda.common.entity;

import com.skanda.common.Constants.CommonConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "返回信息")
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态码", required = true)
    private int code;
    @ApiModelProperty(value = "是否成功", required = true)
    private boolean success;
    @ApiModelProperty(value = "承载数据")
    private T data;
    @ApiModelProperty(value = "返回消息", required = true)
    private String msg;

    /**
     * 返回R
     *
     * @param data 数据
     * @param <T>  T 泛型标记
     * @return R
     */
    public static <T> R<T> data(T data) {
        return new R<>(CommonConstant.DEFAULT_SUCCESS_CODE,true, data, CommonConstant.DEFAULT_SUCCESS_MESSAGE);
    }
    public static R fail(){
        return R.builder().success(false).code(500).msg("操作失败").build();
    }
    public static R fail(String msg){
        return R.builder().success(false).code(500).msg(msg).build();
    }

}

