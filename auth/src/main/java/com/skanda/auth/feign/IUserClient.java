package com.skanda.auth.feign;


import com.skanda.common.entity.R;
import com.skanda.common.entity.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "sys")
public interface IUserClient {

    /**
     * 获取用户信息
     *
     * @param account    账号
     * @param password   密码
     * @return
     */
    @GetMapping("/user-info")
    R<UserInfo> userInfo(@RequestParam("account") String account, @RequestParam("password") String password);

}
